@file:JvmName("Crawler")

package io.krawler

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import io.krawler.conf.RuntimeConf
import io.krawler.conf.parseConf
import org.slf4j.LoggerFactory

interface Node {

  fun start()
}

fun main(args: Array<String>) {
  val log = LoggerFactory.getLogger("io.krawler.Crawler")
  val profiles = profiles()
  val profileName = if (args.size > 0 && !args[0].isEmpty()) args[0] else "standalone"
  val confLocation = if (args.size > 1 && !args[1].isEmpty()) args[1] else "classpath:$profileName.json"
  val profile: Profile? = profiles.filter { it.name == profileName }.firstOrNull()
  if (profile == null) {
    log.error("Unable to find profile {} - candidates are {}", profileName, profiles)
    System.exit(1)
  }

  val conf = parseConf(confLocation)
  val modules = profile!!.modules()
  val kodein = Kodein {
    bind<RuntimeConf>() with instance(RuntimeConf(profileName))
    bind<CrawlerConf>() with instance(conf)
    modules.forEach {
      // import elect modules here
      import(it)
    }
  }

  // Start the node
  log.info("Starting node ...")
  val node = kodein.instance<Node>()
  node.start()
}
