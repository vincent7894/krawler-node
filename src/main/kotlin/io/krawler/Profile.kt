package io.krawler

import com.github.salomonbrys.kodein.Kodein
import io.krawler.cluster.Cluster
import io.krawler.standalone.Standalone


interface Profile {

  val name: String

  fun modules(): List<Kodein.Module>
}

fun profiles(): List<Profile> = listOf(Standalone, Cluster)
