package io.krawler.cluster

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import io.krawler.Node
import io.krawler.Profile
import io.krawler.component.EtcdNodeEntitySync
import io.krawler.component.HealthChecker
import io.krawler.component.Healths
import io.krawler.component.HeartbeatService
import io.krawler.component.HttpService
import io.krawler.component.IHeartbeatService
import io.krawler.component.IHttpService
import io.krawler.component.ILockService
import io.krawler.component.IMasterElectorService
import io.krawler.component.INodeIdentityService
import io.krawler.component.INodeIdentitySync
import io.krawler.component.IScriptingService
import io.krawler.component.ISignalService
import io.krawler.component.LockService
import io.krawler.component.MasterElectorService
import io.krawler.component.NodeIdentityService
import io.krawler.component.ScriptingService
import io.krawler.component.SignalService
import io.krawler.component.mountAll
import io.krawler.http.MasterResource
import io.krawler.http.SignalResource
import org.slf4j.Logger
import org.slf4j.LoggerFactory


private fun clusterModule() = Kodein.Module {
  // Base services
  bind<Node>() with singleton { ClusterNode(kodein) }
  bind<HealthChecker>() with instance(Healths.memory())
  bind<ISignalService>() with singleton { SignalService() }
  bind<ILockService>() with singleton { LockService(kodein) }
  bind<INodeIdentitySync>() with singleton { EtcdNodeEntitySync(kodein) }
  bind<INodeIdentityService>() with singleton { NodeIdentityService(kodein) }

  // Http resources
  bind<SignalResource>() with singleton { SignalResource(kodein) }
  bind<MasterResource>() with singleton { MasterResource(kodein) }

  // Components
  bind<IHttpService>() with singleton { HttpService(kodein) }
  bind<IScriptingService>() with singleton { ScriptingService(kodein) }
  bind<IHeartbeatService>() with singleton { HeartbeatService(kodein) }
  bind<IMasterElectorService>() with singleton { MasterElectorService(kodein) }
}

object Cluster : Profile {

  override val name: String = "cluster"

  override fun modules(): List<Kodein.Module> = listOf(clusterModule())

  override fun toString(): String = name
}


// NODE

class ClusterNode(override val kodein: Kodein) : Node, KodeinAware {

  val log: Logger = LoggerFactory.getLogger(javaClass)

  val http: IHttpService = instance()
  val scripting: IScriptingService = instance()
  val heartbeat: IHeartbeatService = instance()
  val masterElector: IMasterElectorService = instance()

  override fun start() {
    listOf(http, scripting, heartbeat, masterElector).mountAll()
    log.info("Node is running")
  }
}

