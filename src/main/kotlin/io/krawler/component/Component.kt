package io.krawler.component

interface Component {

  fun mount()

  fun umount() {}
}
