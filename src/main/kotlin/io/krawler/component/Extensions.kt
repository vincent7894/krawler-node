@file:JvmName("Extensions")

package io.krawler.component

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

/**
 * Kotlin aware object mapper thanks to a jackson kotlin module
 *
 */
val json = ObjectMapper().registerModule(KotlinModule())

/**
 * Serialize and Unserialize json extension methods
 *
 */

fun <T> T.toJson(): String = json.writeValueAsString(this)

inline fun <reified T> String.fromJson(): T = json.readValue(this, T::class.java)

inline fun <reified T> ByteArray.fromJson(): T = json.readValue(this, T::class.java)


/**
 * Component extensions
 *
 */

fun List<Component>.mountAll() = this.forEach { cmp ->
  cmp.mount()
}
