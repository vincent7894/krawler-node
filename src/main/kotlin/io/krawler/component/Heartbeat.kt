package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.timerTask


interface IHeartbeatService : Component {

  fun pause()
  fun upause()
  fun refresh()
}

interface HealthChecker {

  fun check(): Health
}

enum class Health(val status: Int) {
  UP(200),
  UNSTABLE(403),
  DOWN(403)
}

class HeartbeatService(override val kodein: Kodein) : IHeartbeatService, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  val cfg: CrawlerConf = instance()
  val healthChecker: HealthChecker = instance()
  val identitySync: INodeIdentitySync = instance()
  val identityService: INodeIdentityService = instance()

  private val pause: AtomicBoolean = AtomicBoolean(false)
  private val timer by lazy {
    Timer(cfg.heartbeat.timerName)
  }

  override fun pause() = pause.set(true)
  override fun upause() = pause.set(false)
  override fun mount() {
    if (!cfg.heartbeat.enabled) {
      log.info("Heartbeat is disabled")
      return
    }

    timer.schedule(timerTask {
      if (!pause.get()) {
        refresh()
      }
    }, cfg.heartbeat.delay, cfg.heartbeat.period)
  }


  override fun umount() {
    pause()
    timer.cancel()
  }

  override fun refresh() {
    identitySync.sync(healthChecker.check(), identityService.myself())
  }
}

// Helper

object Healths {

  fun memory() = object : HealthChecker {

    override fun check(): Health {
      val percent = MemoryHelper.snapshot().percent()
      val percentAvailable = 100 - percent
      return when {
        percent <= 70 -> Health.UP
        percentAvailable in 15 until 30 -> Health.UNSTABLE
        else -> Health.DOWN
      }
    }
  }

  fun lambda(call: () -> Health) = object : HealthChecker {

    override fun check(): Health = call()
  }
}
