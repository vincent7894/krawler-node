package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.instanceOrNull
import io.krawler.conf.CrawlerConf
import io.krawler.http.MasterResource
import io.krawler.http.SignalResource
import org.jetbrains.ktor.host.ApplicationHost
import org.jetbrains.ktor.host.embeddedServer
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.netty.Netty
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get
import org.jetbrains.ktor.routing.routing
import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface IHttpService : Component

class HttpService(override val kodein: Kodein) : IHttpService, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Injected

  val cfg: CrawlerConf = instance()
  val healthChecker: HealthChecker? = instanceOrNull()

  // Resource

  val signalResource: SignalResource? = instanceOrNull()
  val masterResource: MasterResource? = instanceOrNull()

  // Custom lazy

  private val server: ApplicationHost by lazy {
    log.info("Bind http on {}:{}", cfg.node.host, cfg.node.port)

    embeddedServer(Netty, cfg.node.port, cfg.node.host) {
      routing {
        // BEGIN routing

        masterResource?.bind(this)
        signalResource?.bind(this)

        bindHealthEndpoint()

        // EOF routing
      }
    }
  }

  override fun mount() {
    if (cfg.node.headless) {
      log.info("Http is disabled")
      return
    }
    server.start()
  }

  override fun umount() {
    if (!cfg.node.headless) server.start(true)
  }

  // Bind defaults

  private fun Routing.bindHealthEndpoint() {
    if (healthChecker != null) {
      get(healthUri) {
        val health = healthChecker.check()
        it.response.status(HttpStatusCode.fromValue(health.status))
        it.respondText(health.name, ContentType.parse(healthContentType))
      }
    }
  }

  companion object {

    val healthUri = "/health"
    val healthContentType = "text/plain"
  }
}

