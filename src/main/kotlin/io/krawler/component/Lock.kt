package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import mousio.etcd4j.EtcdClient
import mousio.etcd4j.responses.EtcdErrorCode
import mousio.etcd4j.responses.EtcdException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.URI

interface ILockService {

  fun lock(id: String, value: String): Boolean
  fun unlock(id: String, value: String): Boolean
  fun isLocked(id: String): Boolean
}

class LockService(override val kodein: Kodein) : ILockService, KodeinAware {

  // Injected

  private val cfg: CrawlerConf = instance()

  // Customs lazy

  private val etcd by lazy {
    EtcdClient(URI.create(cfg.etcd.host))
  }

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  override fun lock(id: String, value: String): Boolean = try {
    etcd.put(pathOf(id), value).prevExist(false).ttl(240).send().get() != null
  } catch(e: EtcdException) {
    if (e.errorCode != EtcdErrorCode.NodeExist) {
      throw RuntimeException(e)
    } else {
      log.warn("Already lock id:{}, value:{}", id, value)
    }
    false
  }

  override fun unlock(id: String, value: String): Boolean = try {
    etcd.delete(pathOf(id)).prevValue(value).send().get() != null
  } catch(e: EtcdException) {
    if (e.errorCode != EtcdErrorCode.PrevValueRequired) {
      throw RuntimeException(e)
    } else {
      log.warn("Nothing to unlock id:{}, value:{}", id, value)
    }
    false
  }

  override fun isLocked(id: String): Boolean = try {
    etcd.get(pathOf(id)).send().get() != null
  } catch(e: EtcdException) {
    if (e.errorCode != EtcdErrorCode.KeyNotFound) {
      throw RuntimeException(e)
    } else {
      log.warn("Nothing to check id:{}", id)
    }
    false
  }

  private fun pathOf(id: String): String = cfg.etcd.lockKey + "/" + id.toLowerCase()
}