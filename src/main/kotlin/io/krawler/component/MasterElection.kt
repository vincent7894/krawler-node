package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import org.slf4j.Logger
import org.slf4j.LoggerFactory


data class Node(
    val id: String,
    val uptime: Long,
    val ip: String,
    val httpPort: Int,
    val shutdownPort: Int,
    val profile: String,
    val tags: List<String>,
    val responsibility: NodeResponsibility,
    val memory: NodeMemory) {

  //val dispatchable: Boolean = !responsibility.masterOnly
  val eligible: Boolean = !responsibility.master && (responsibility.masterOnly || !responsibility.workerOnly)

  fun copyAsMaster() = copy(responsibility = responsibility.copy(master = true))
}

/**
 * Utils classes to describe a Node
 *
 */

data class NodeResponsibility(
    val master: Boolean,
    val masterOnly: Boolean,
    val workerOnly: Boolean)

data class NodeMemory(
    val totalMemory: Long,
    val usedMemory: Long,
    val freeMemory: Long) {

  fun percent(): Int {
    val ratio = usedMemory * 100 / totalMemory
    return ratio.toInt()
  }
}

data class ElectionResult(
    val locked: Boolean = false,
    val master: Node? = null
) {

  val elected = master != null
}

object MemoryHelper {

  fun snapshot(): NodeMemory {
    val freeMemory = Runtime.getRuntime().freeMemory()
    val maxMemory = Runtime.getRuntime().maxMemory()
    val totalMemory = Runtime.getRuntime().totalMemory()
    return NodeMemory(maxMemory, totalMemory, freeMemory)
  }
}

/**
 * Master election
 *
 */

interface IMasterElectorService : Component

class MasterElectorService(override val kodein: Kodein) : IMasterElectorService, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Injected

  private val cfg: CrawlerConf = instance()
  private val lockService: ILockService = instance()
  private val identitySync: INodeIdentitySync = instance()
  private val identityService: INodeIdentityService = instance()

  // Custom

  private val lockId = "masterElection"

  override fun mount() {
    if (!cfg.node.masterElection) {
      log.info("Master election is disabled")
      return
    }

    identitySync.watch().doOnError {
      log.error("Error whe trying to elect master", it)
    }.map map@ { nodes ->
      log.debug("Receive {} nodes", nodes.size)

      if (nodes.isEmpty()) {
        return@map ElectionResult()
      }

      val myself = identityService.myself()
      val runningMaster = runningMaster(nodes, myself)
      toElectionResult(runningMaster, myself, nodes)
    }.subscribe {
      if (it.elected) {
        log.info("Master elected")
        identitySync.sync(it.master!!.copyAsMaster())
      }
      // Release lock
      if (it.locked) {
        lockService.unlock(lockId, identityService.myself().id)
        log.debug("Master election unlocked")
      }
    }
  }

  private fun runningMaster(nodes: Nodes, myself: Node): Node? {
    val runningMaster = nodes.filter {
      it.responsibility.master
    }.firstOrNull()
    if (myself.id == runningMaster?.id && !identityService.isMaster()) {
      identityService.master() // set current node to master
    } else {
      identityService.umaster()
    }
    return runningMaster
  }

  private fun toElectionResult(runningMaster: Node?, myself: Node, nodes: Nodes): ElectionResult {
    if (runningMaster == null) {
      log.debug("{} try to acquire {}", myself.id, lockId)
      val locked = lockService.lock(lockId, myself.id)
      if (locked) {
        val newMaster = nodes.elect(ElectionStrategy.DEFAULT)
        log.info("New master - {}", newMaster)
        if (newMaster == null) {
          log.info("Unable to elect a new master")
        }
        return ElectionResult(locked, newMaster)
      }
    } else {
      log.debug("Master already exists - nothing to do")
    }
    return ElectionResult()
  }
}

enum class ElectionStrategy {
  DEFAULT, TINIEST, OLDEST
}

inline fun List<Node>.elect(electionStrategy: ElectionStrategy): Node? = when (electionStrategy) {
  ElectionStrategy.DEFAULT -> elect(this)
  ElectionStrategy.TINIEST -> electTiniest(this)
  ElectionStrategy.OLDEST -> electOldest(this)
}

/**
 * Election implementations
 *
 */

fun elect(nodes: List<Node>): Node? {
  val freeMemory = Comparator.comparing<Node, Boolean> { it.memory.percent() <= 35 }
  val totalMemory = Comparator.comparing<Node, Long> { it.memory.totalMemory }.reversed()
  val masterOnly = Comparator.comparing<Node, Boolean> { it.responsibility.masterOnly }.reversed()
  val uptime = Comparator.comparing<Node, Long>(Node::uptime).reversed()
  val comparator = masterOnly.then(freeMemory).then(totalMemory).then(uptime)
  return nodes
      .filter(Node::eligible)
      .sortedWith(comparator)
      .firstOrNull()
}

fun electOldest(nodes: List<Node>): Node? {
  val masterOnly = Comparator.comparing<Node, Boolean> { it.responsibility.masterOnly }.reversed()
  val uptime = Comparator.comparing<Node, Long>(Node::uptime)
  return nodes.sortedWith(masterOnly.then(uptime)).firstOrNull()
}

fun electTiniest(nodes: List<Node>): Node? {
  val masterOnly = Comparator.comparing<Node, Boolean> { it.responsibility.masterOnly }.reversed()
  val totalMemory = Comparator.comparing<Node, Long> { it.memory.totalMemory }
  return nodes.sortedWith(masterOnly.then(totalMemory)).firstOrNull()
}