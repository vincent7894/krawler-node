package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import io.krawler.conf.RuntimeConf
import mousio.etcd4j.EtcdClient
import org.reactivestreams.Publisher
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import java.net.URI
import java.util.concurrent.atomic.AtomicBoolean

interface INodeIdentityService {

  fun myself(): Node

  fun master()
  fun umaster()
  fun isMaster(): Boolean
}

class NodeIdentityService(override val kodein: Kodein) : INodeIdentityService, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Injected

  private val cfg: CrawlerConf = instance()
  private val rcfg: RuntimeConf = instance()

  // Custom

  private val uptime = System.currentTimeMillis()
  private val master: AtomicBoolean = AtomicBoolean(false)

  override fun myself(): Node = Node(
      cfg.uniqueId,
      uptime,
      cfg.node.host,
      cfg.node.port,
      cfg.node.shutdownPort,
      rcfg.profile,
      cfg.node.tags,
      NodeResponsibility(master.get(), cfg.node.masterOnly, cfg.node.workerOnly),
      MemoryHelper.snapshot())

  override fun isMaster(): Boolean = master.get()

  override fun umaster() = master.set(false)

  override fun master() {
    master.set(true)
    log.info("I'm ({}) the new master", cfg.uniqueId)
  }

}

typealias Nodes = List<Node>

interface INodeIdentitySync {

  fun watch(): Flux<Nodes>
  fun sync(node: Node)
  fun sync(health: Health, node: Node)
}

class EtcdNodeEntitySync(override val kodein: Kodein) : INodeIdentitySync, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Injected

  private val cfg: CrawlerConf = instance()

  // Custom

  private var key: String? = null
  private val watch: AtomicBoolean = AtomicBoolean(true)
  private val sc = Schedulers.newSingle("nodes-watcher")

  // Customs lazy

  private val etcd by lazy { EtcdClient(URI.create(cfg.etcd.host)) }
  private val publisher: Publisher<Nodes> by lazy {
    watcher()
  }

  override fun watch(): Flux<Nodes> = Flux.from(publisher).subscribeOn(sc)

  private fun watcher(): Publisher<Nodes> = Publisher<Nodes> { subscriber ->
    while (watch.get()) {
      log.debug("Wait for nodes modifications")

      // Used to lock until nodes changes
      etcd.get(cfg.etcd.directory).waitForChange().recursive().send().get()

      val response = etcd.get(cfg.etcd.directory).recursive().send().get()
      val nodes = if (response.node.nodes.isEmpty()) {
        listOf(response.node.value.fromJson<Node>())
      } else {
        response.node.nodes.map {
          it.value.fromJson<Node>()
        }
      }

      log.debug("Nodes change occurred on etcd directory ({}) - {}", cfg.etcd.directory, nodes)
      subscriber.onNext(nodes)
    }

    subscriber.onComplete()
  }

  override fun sync(node: Node) {
    save(node, false)
  }

  override fun sync(health: Health, node: Node) {
    log.debug("Sync node - health:{}, id:{}", health, node.id)
    return when (key) {
      null -> save(node)
      else -> update(health, node)
    }
  }

  private fun save(node: Node, cacheKey: Boolean = true) {
    val put = etcd.put(cfg.etcd.directory + "/" + node.id, node.toJson())
        .ttl(cfg.etcd.expires)
        .send()
        .get()
    if (cacheKey) key = put.node.key
  }

  private fun update(health: Health, node: Node) = when (health) {
    Health.DOWN -> delete()
    Health.UP, Health.UNSTABLE -> save(node)
  }

  private fun delete() {
    if (key != null) {
      etcd.delete(key).send().get()
      key = null
    }
  }
}
