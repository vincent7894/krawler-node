package io.krawler.component

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.conf.CrawlerConf
import org.slf4j.Logger
import org.slf4j.LoggerFactory


interface IScriptingService : Component


class ScriptingService(override val kodein: Kodein) : IScriptingService, KodeinAware {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Inject

  private val cfg: CrawlerConf = instance()

  override fun mount() {
    log.info("Compile scripts in {}", cfg.scripting.folders)
    // TODO
    log.info("Load scripts")
    // TODO
  }
}