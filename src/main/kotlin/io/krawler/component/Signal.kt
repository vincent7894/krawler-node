package io.krawler.component

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Flux
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers


enum class SignalType {
  PAUSE_HEARTBEAT
}

data class Signal(
    val type: SignalType,
    val time: Long = System.currentTimeMillis())


interface ISignalService {

  fun push(signal: Signal)

  fun watch(): Flux<Signal>
}

class SignalService : ISignalService {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  // Custom

  val sc: Scheduler = Schedulers.newSingle("signal")
  val emitter: EmitterProcessor<Signal> = EmitterProcessor.create<Signal>().connect()
  val flux: Flux<Signal> = emitter.publishOn(sc)

  /*init {
    watch().doOnEach {
      println("GOT one - " + it.get().type)
    }.subscribe {
      println(Thread.currentThread().name + " - got signal - " + it)
      Thread.sleep(20_000)
    }
  }*/

  override fun watch(): Flux<Signal> = flux

  override fun push(signal: Signal) = emitter.onNext(signal)
}
