package io.krawler.conf


data class EtcdConf(
    val host: String = "http://127.0.0.1:2379",
    val directory: String = "/krawler-nodes",
    val lockKey: String = "/locks",
    val expires: Int = 160)

data class HeartbeatConf(
    val enabled: Boolean = true,
    val delay: Long = 10_000,
    val period: Long = 30_000,
    val timerName: String = "heartbeat")