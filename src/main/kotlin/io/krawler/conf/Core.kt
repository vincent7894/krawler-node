package io.krawler.conf

import io.krawler.component.fromJson
import java.io.FileInputStream
import java.io.InputStream
import java.util.*


data class CrawlerConf(
    val node: NodeConf = NodeConf(),
    val etcd: EtcdConf = EtcdConf(),
    val heartbeat: HeartbeatConf = HeartbeatConf(),
    val scripting: ScriptingConf = ScriptingConf()) {

  // Custom lazy

  val uniqueId by lazy {
    UUID.randomUUID().toString().replace("-", "")
  }
}

data class NodeConf(
    val host: String = "localhost",
    val port: Int = 6002,
    val shutdownPort: Int = port + 10,
    val headless: Boolean = false,
    val masterElection: Boolean = true,
    val masterOnly: Boolean = false,
    val workerOnly: Boolean = false,
    val tags: List<String> = listOf())

data class RuntimeConf(val profile: String)

data class ScriptingConf(
    val folders: List<String> =
    listOf("/opt/crawler/scripting", System.getProperty("user.home") + "/crawler/scripting", "./scripting"))

fun parseConf(path: String): CrawlerConf {
  val file = "file:"
  val classpath = "classpath:"
  val cpIdx = if (path.startsWith(classpath)) classpath.length else -1
  val fileIdx = if (path.startsWith(file)) file.length else 0
  val idx = if (cpIdx != -1) cpIdx else fileIdx
  val realPath = path.substring(idx)
  val stream: InputStream = if (cpIdx != -1) {
    val p = if (realPath.startsWith("/")) realPath else "/" + realPath
    CrawlerConf::class.java.getResourceAsStream(p)
  } else {
    FileInputStream(realPath)
  }

  val bytes = stream.use {
    it.readBytes()
  }
  return bytes.fromJson<CrawlerConf>()
}