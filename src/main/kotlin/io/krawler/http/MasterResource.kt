package io.krawler.http

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.component.INodeIdentityService
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.get


class MasterResource(override val kodein: Kodein) : KodeinAware {

  // Inject

  val nodeIdentityService: INodeIdentityService = instance()

  fun bind(routing: Routing) {
    with(routing) {
      get(MasterResource.root) {
        it.master()
      }
    }
  }

  suspend fun ApplicationCall.master() {
    respondText(nodeIdentityService.isMaster().toString(), ContentType.parse("text/plain"))
  }

  companion object {

    val root = "/master"
  }
}
