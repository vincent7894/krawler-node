package io.krawler.http

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.instance
import io.krawler.component.ISignalService
import io.krawler.component.Signal
import io.krawler.component.fromJson
import org.jetbrains.ktor.application.ApplicationCall
import org.jetbrains.ktor.http.ContentType
import org.jetbrains.ktor.http.HttpStatusCode
import org.jetbrains.ktor.response.respondText
import org.jetbrains.ktor.routing.Routing
import org.jetbrains.ktor.routing.post

class SignalResource(override val kodein: Kodein) : KodeinAware {

  // Inject

  val signalService: ISignalService = instance()

  fun bind(routing: Routing) {
    with(routing) {
      post(root) {
        it.signals()
      }
    }
  }

  suspend fun ApplicationCall.signals() {
    val received = request.receive(String::class)
    signalService.push(received.fromJson<Signal>())
    response.status(HttpStatusCode.Accepted)
    respondText("ACK", ContentType.parse("text/plain"))
  }

  companion object {

    val root = "/signals"
  }
}

