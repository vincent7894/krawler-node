package io.krawler.standalone

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.KodeinAware
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.singleton
import io.krawler.Node
import io.krawler.Profile
import io.krawler.cluster.Cluster
import io.krawler.component.HttpService
import io.krawler.component.IHttpService
import org.slf4j.LoggerFactory

private fun standaloneModule() = Kodein.Module {
  bind<Node>() with singleton { StandaloneNode(kodein) }
  // Components
  bind<IHttpService>() with singleton { HttpService(kodein) }
}

object Standalone : Profile {

  override val name: String = "standalone"
  override fun modules(): List<Kodein.Module> = listOf(standaloneModule())
  override fun toString(): String = Cluster.name
}

class StandaloneNode(override val kodein: Kodein) : Node, KodeinAware {

  val http: IHttpService = instance()

  val log = LoggerFactory.getLogger(javaClass)

  // Mount components
  override fun start() {
    http.mount()
    log.info("Node is running")
  }
}
